[![build status](https://gitlab.com/Mumba/node-microservice-sourced/badges/master/build.svg)](https://gitlab.com/Mumba/node-microservice-sourced/commits/master)
# Mumba Microservice Sourced

A Mumba Microservice module that provides Event Sourcing (via [mateodelnorte/sourced](https://github.com/mateodelnorte/sourced) and [mateodelnorte/sourced-repo-mongo](https://github.com/mateodelnorte/sourced-repo-mongo)) support.

## Configuration

This module requires that a `Config` object has been registered as `config`.

Property | Type | Description
--- | :---: | ---
`mongo` | `object` | A dictionary of options required for RabbitMQ.
`mongo:url` | `string` | The URL of the MongoDB server (for example `mongodb://mongo:27017/local`). 

## DI Container

This module adds the following container artifacts if they don't already exist.

Name | Type | Description
--- | :---: | ---
`mongo` | `object` | An instance of a Node.js [`mongo`](https://mongodb.github.io/node-mongodb-native/) driver.

## Installation

```sh
$ npm install --save mumba-microservice-sourced
$ typings install --save dicontainer=npm:mumba-typedef-dicontainer
$ typings install --save sourced=npm:mumba-typedef-sourced
```

## Examples

TODO

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm run docker:up
$ npm install
$ npm test
$ npm docker:down
```

## People

The original author of _Mumba Microservice Service Bus_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-sourced/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

