/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Config} from "mumba-config";
import testConfig from "./config";

/**
 * Creates a Config object for testing.
 *
 * @param {object} [overrides]
 * @returns {Config}
 */
export function createConfig(overrides: any = {}): Config {
	let config = new Config();

	return config.merge(testConfig)
		.merge(overrides);
}
