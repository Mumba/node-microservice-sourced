/**
 * Test configuration.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

let testConfig = {
	mongo: {
		url: 'mongodb://mongo:27017/local'
	}
};

export default testConfig;
