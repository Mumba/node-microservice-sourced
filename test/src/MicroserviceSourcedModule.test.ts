/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from "assert";
import {DiContainer} from "dicontainer";
import {MicroserviceSourcedModule} from '../../src/index';
import {createConfig} from "../bootstrap";

describe('MicroserviceSourcedModule integration tests', () => {
	it('should register "mongo" in the container', () => {
		let container: DiContainer = new Sandal();

		MicroserviceSourcedModule.register(container);

		assert.strictEqual(container.has('mongo'), true, 'should add mongo');
	});

	it('should init mongo', () => {
		let config = createConfig();

		return MicroserviceSourcedModule.init(config)
			.then((db: any) => {
				// All good.
				db.close();
			});
	});
});
