/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const mongo = require('sourced-repo-mongo/mongo');
import {Config} from "mumba-config";
import {DiContainer} from "dicontainer";

/**
 * ServiceBus module.
 */
export class MicroserviceSourcedModule {
	/**
	 * Register the module assets in the DI container.
	 *
	 * @param {DiContainer} container
	 */
	static register(container: DiContainer) {
		container.object('mongo', mongo);
	}

	/**
	 * Initialise the module.
	 *
	 * @param {Config} config
	 * @returns {Promise<void>}
	 */
	static init(config: Config) {
		return new Promise<void>((resolve) => {
			mongo.once('connected', (db: any) => resolve(db));
			mongo.connect(config.get('mongo:url'));
		});
	}
}

