/**
 * Microservice public exports.
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

export {MicroserviceSourcedModule} from "./MicroserviceSourcedModule";
